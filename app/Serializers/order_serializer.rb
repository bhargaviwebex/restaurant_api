class OrderSerializer
  include FastJsonapi::ObjectSerializer
  attributes :quantity, :total
end
