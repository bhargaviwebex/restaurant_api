class Payment < ApplicationRecord
  belongs_to :customer
  belongs_to :order
  validates :customer_id, presence: true
  validates :order_id, presence: true 
  validates :order_id, uniqueness: true
  validates :total, presence: true
end
