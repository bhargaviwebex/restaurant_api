class Restarant < ApplicationRecord
	belongs_to :user
	has_one_attached :image
	validates :user_id, uniqueness: true
	validates :name, presence: true
	validates :email, uniqueness: true, format: { with: URI::MailTo::EMAIL_REGEXP } 
	validates :address, presence: true
	validates :contact, uniqueness: true
	validates :location, presence: true
	has_many :items
end
