Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  post 'users', to: 'users#create'

  get 'users', to: 'users#index'

  get 'users/:id', to: 'users#show'

  post 'login', to: 'users#login'

  delete 'users/:id', to: 'users#delete'

  delete 'logout', to: 'users#logout'

  get 'restarants', to: 'restarants#index'

  get 'restarants/:id', to: 'restarants#show'

  post 'restarants', to: 'restarants#create'

  put 'restarants/:id', to: 'restarants#update'

  delete 'restarants/:id', to: 'restarants#delete'

  resources :customers

  get 'user/get_restarants', to: 'customers#get_restarant'

  get 'user/get_items', to: 'customers#get_item'


  resources :items

  resources :orders

  resources :payments
  


  
end
