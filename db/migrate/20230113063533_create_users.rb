class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :password_digest
      t.text :address
      t.bigint :account_number
      t.string :upi_id
      t.text :gst_number

      t.timestamps
    end
  end
end
